<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

// User Routes

Route::get('/', 'UserHomeController@go');

Route::get('index', 'UserHomeController@go');
Route::get('about', 'AboutController@go');
Route::get('Academics', 'AcademicsController@go');
Route::get('Admission', 'AdmissionController@go');
Route::get('Sports', 'SportsController@go');
Route::get('Contacts', 'ContactsController@go');
Route::get('Research', 'ResearchController@go');
Route::get('researchList', 'ResearchController@fullList');
Route::get('Undergrad', 'UndergradsController@go');
Route::get('Grad', 'GradsController@go');

Route::get('Campusnews', 'CampusnewsController@go');
Route::get('Shownews/{newsID}', 'CampusnewsController@showNews');

Route::get('Campusevents', 'CampuseventsController@go');
Route::get('Showevents/{eventsID}', 'CampuseventsController@showEvents');

Route::get('football', 'SportsController@football');
Route::get('cricket', 'SportsController@cricket');
Route::get('volleyball', 'SportsController@volleyball');

Route::get('spotlight', 'SpotlightController@go');

Route::get('Staffs', 'FacultyPeopleController@staffs');
Route::get('Members', 'FacultyPeopleController@members');

Route::get('cluster', 'ClusterController@go');

Route::get('Students', 'StdAlmController@students');
Route::get('Alumni', 'StdAlmController@alumni');



//Admin Routes

Auth::routes();

Route::get('profile', 'UserController@profile');

Route::get('/home', 'AdminHomeController@index');

Route::get('uploadNews', 'UploadController@news')->middleware('auth');
Route::post('uploadNews', 'UploadController@addnews');

Route::get('uploadEvents', 'UploadController@events')->middleware('auth');
Route::post('uploadEvents', 'UploadController@addevents');

Route::get('updateFootball', 'UpdateSportsController@football')->middleware('auth');
Route::patch('updateFootball', 'UpdateSportsController@updateFootball');

Route::get('updateCricket', 'UpdateSportsController@cricket')->middleware('auth');
Route::patch('updateCricket', 'UpdateSportsController@updateCricket');

Route::get('updateVolleyball', 'UpdateSportsController@volleyball')->middleware('auth');
Route::patch('updateVolleyball', 'UpdateSportsController@updateVolleyball');

Route::get('updateContacts/{contactID}', 'UpdateContactsController@go')->middleware('auth');
Route::patch('updateContacts/{contactID}', 'UpdateContactsController@updateContacts');

Route::get('updateSpotlight', 'UpdateSpotlightController@go')->middleware('auth');
Route::patch('updateSpotlight', 'UpdateSpotlightController@updateSpotlight');

Route::get('addStaff', 'StaffController@go')->middleware('auth');
Route::post('addStaff', 'StaffController@addStaff');

Route::get('updateStaff', 'StaffController@goUP')->middleware('auth');
Route::get('updateStaff/{byID}', 'StaffController@goUPbyID')->middleware('auth');
Route::patch('updateStaff/{byID}', 'StaffController@updateByID');

Route::get('addMember', 'MemberController@go')->middleware('auth');
Route::post('addMember', 'MemberController@addMember');

Route::get('updateMember', 'MemberController@goUP')->middleware('auth');
Route::get('updateMember/{byID}', 'MemberController@goUPbyID')->middleware('auth');
Route::patch('updateMember/{byID}', 'MemberController@updateByID');

Route::get('updateStudents', 'StdAlmController@studentsUP')->middleware('auth');
Route::patch('updateStudents', 'StdAlmController@updateStudents');

Route::get('updateAlumni', 'StdAlmController@alumniUP')->middleware('auth');
Route::patch('updateAlumni', 'StdAlmController@updateAlumni');

Route::get('updateUndergrad', 'UndergradsController@goUP')->middleware('auth');
Route::patch('updateUndergrad', 'UndergradsController@update');

Route::get('updateGrad', 'GradsController@goUP')->middleware('auth');
Route::patch('updateGrad', 'GradsController@update');

Route::get('addResearch', 'ResearchController@goUP')->middleware('auth');
Route::post('addResearch', 'ResearchController@add');


