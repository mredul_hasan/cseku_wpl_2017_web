$(function(){ 

    $('a.view_more').on('click',function(event){ 

        event.preventDefault(); 
        $(this).hide();
        $(this).parent().find('.more_text').show();

    });

     $('a.view_less').on('click',function(event){ 

        event.preventDefault(); 
        $(this).parent().hide();
        $('a.view_more').show();

    });

});