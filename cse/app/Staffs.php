<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Staffs extends Model
{
    public function staffs(){
    	return $this->hasMany(Staffs::class);
    }
}
