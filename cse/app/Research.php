<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Research extends Model
{
    protected $table = 'research';

    public function researches(){
    	return $this->hasMany(Research ::class);
    }
}
