<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Members extends Model
{
    public function members(){
    	return $this->hasMany(Members::class);
    }
}
