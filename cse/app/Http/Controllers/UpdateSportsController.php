<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use DB;
use Image;

class UpdateSportsController extends Controller
{
    public function football(){
    	$data = DB::table('sports')->where('sports_id', 1)->first();
    	return view('sports.updateFootball', compact('data'));
    }

    public function updateFootball(Request $request){
    	 //validation
        $this->validate($request, [
        		'title' => 'required',
        		'body' => 'required',
                'avatar1' => 'dimensions:min_width=1011,min_height=370 | image',
                'avatar2' => 'dimensions:min_width=1011,min_height=370 | image',
                'avatar3' => 'dimensions:min_width=1011,min_height=370 | image',
                'avatar4' => 'dimensions:min_width=1011,min_height=370 | image',
                'avatar5' => 'dimensions:min_width=1011,min_height=370 | image',

            ]);

        DB::table('sports')->where('sports_id', 1)->update(['title' => $request->title]);
        DB::table('sports')->where('sports_id', 1)->update(['body' => $request->body]);

        $data = DB::table('sports')->where('sports_id', 1)->first();  //get the old datas for replacing
        
        if($request->hasFile('avatar1')){
        	
            $avatar = $request->file('avatar1');
            $filename = time() . '1.' . $avatar->getClientOriginalExtension();
            Image::make($avatar)->resize(1011, 370)->save(public_path('/uploads/sports/football/' . $filename));
            DB::table('sports')->where('sports_id', 1)->update(['avatar1' => $filename]);
            unlink(public_path('uploads/sports/football/') . $data->avatar1); //removing old image
         }
         if($request->hasFile('avatar2')){
         	
            $avatar = $request->file('avatar2');
            $filename = time() . '2.' . $avatar->getClientOriginalExtension();
            Image::make($avatar)->resize(1011, 370)->save(public_path('/uploads/sports/football/' . $filename));
            DB::table('sports')->where('sports_id', 1)->update(['avatar2' => $filename]);
            unlink(public_path('uploads/sports/football/') . $data->avatar2); //removing old image
         }
         if($request->hasFile('avatar3')){
         	
            $avatar = $request->file('avatar3');
            $filename = time() . '3.' . $avatar->getClientOriginalExtension();
            Image::make($avatar)->resize(1011, 370)->save(public_path('/uploads/sports/football/' . $filename));
            DB::table('sports')->where('sports_id', 1)->update(['avatar3' => $filename]);
            unlink(public_path('uploads/sports/football/') . $data->avatar3); //removing old image
         }
         if($request->hasFile('avatar4')){
         	
            $avatar = $request->file('avatar4');
            $filename = time() . '4.' . $avatar->getClientOriginalExtension();
            Image::make($avatar)->resize(1011, 370)->save(public_path('/uploads/sports/football/' . $filename));
            DB::table('sports')->where('sports_id', 1)->update(['avatar4' => $filename]);
            unlink(public_path('uploads/sports/football/') . $data->avatar4); //removing old image
         }
         if($request->hasFile('avatar5')){
            $avatar = $request->file('avatar5');
            $filename = time() . '5.' . $avatar->getClientOriginalExtension();
            Image::make($avatar)->resize(1011, 370)->save(public_path('/uploads/sports/football/' . $filename));
            DB::table('sports')->where('sports_id', 1)->update(['avatar5' => $filename]);
            unlink(public_path('uploads/sports/football/') . $data->avatar5); //removing old image
         }

         return redirect()->back()->with('success', 'Updated Successfully.');
  

    }

    public function cricket(){
    	$data = DB::table('sports')->where('sports_id', 2)->first();
    	return view('sports.updateCricket', compact('data'));
    } 

     public function updateCricket(Request $request){
    	 //validation
        $this->validate($request, [
        		'title' => 'required',
        		'body' => 'required',
                'avatar1' => 'dimensions:min_width=1011,min_height=370 | image',
                'avatar2' => 'dimensions:min_width=1011,min_height=370 | image',
                'avatar3' => 'dimensions:min_width=1011,min_height=370 | image',
                'avatar4' => 'dimensions:min_width=1011,min_height=370 | image',
                'avatar5' => 'dimensions:min_width=1011,min_height=370 | image',

            ]);

        DB::table('sports')->where('sports_id', 2)->update(['title' => $request->title]);
        DB::table('sports')->where('sports_id', 2)->update(['body' => $request->body]);

        $data = DB::table('sports')->where('sports_id', 2)->first();  //get the old datas for replacing
        
        if($request->hasFile('avatar1')){
        	
            $avatar = $request->file('avatar1');
            $filename = time() . '1.' . $avatar->getClientOriginalExtension();
            Image::make($avatar)->resize(1011, 370)->save(public_path('/uploads/sports/cricket/' . $filename));
            DB::table('sports')->where('sports_id', 2)->update(['avatar1' => $filename]);
            unlink(public_path('uploads/sports/cricket/') . $data->avatar1); //removing old image
         }
         if($request->hasFile('avatar2')){
         	
            $avatar = $request->file('avatar2');
            $filename = time() . '2.' . $avatar->getClientOriginalExtension();
            Image::make($avatar)->resize(1011, 370)->save(public_path('/uploads/sports/cricket/' . $filename));
            DB::table('sports')->where('sports_id', 2)->update(['avatar2' => $filename]);
            unlink(public_path('uploads/sports/cricket/') . $data->avatar2); //removing old image
         }
         if($request->hasFile('avatar3')){
         	
            $avatar = $request->file('avatar3');
            $filename = time() . '3.' . $avatar->getClientOriginalExtension();
            Image::make($avatar)->resize(1011, 370)->save(public_path('/uploads/sports/cricket/' . $filename));
            DB::table('sports')->where('sports_id', 2)->update(['avatar3' => $filename]);
            unlink(public_path('uploads/sports/cricket/') . $data->avatar3); //removing old image
         }
         if($request->hasFile('avatar4')){
         	
            $avatar = $request->file('avatar4');
            $filename = time() . '4.' . $avatar->getClientOriginalExtension();
            Image::make($avatar)->resize(1011, 370)->save(public_path('/uploads/sports/cricket/' . $filename));
            DB::table('sports')->where('sports_id', 2)->update(['avatar4' => $filename]);
            unlink(public_path('uploads/sports/cricket/') . $data->avatar4); //removing old image
         }
         if($request->hasFile('avatar5')){
            $avatar = $request->file('avatar5');
            $filename = time() . '5.' . $avatar->getClientOriginalExtension();
            Image::make($avatar)->resize(1011, 370)->save(public_path('/uploads/sports/cricket/' . $filename));
            DB::table('sports')->where('sports_id', 2)->update(['avatar5' => $filename]);
            unlink(public_path('uploads/sports/cricket/') . $data->avatar5); //removing old image
         }

         return redirect()->back()->with('success', 'Updated Successfully.');
  

    }    

    public function volleyball(){
    	$data = DB::table('sports')->where('sports_id', 3)->first();
    	return view('sports.updateVolleyball', compact('data'));
    }

     public function updateVolleyball(Request $request){
    	 //validation
        $this->validate($request, [
        		'title' => 'required',
        		'body' => 'required',
                'avatar1' => 'dimensions:min_width=1011,min_height=370 | image',
                'avatar2' => 'dimensions:min_width=1011,min_height=370 | image',
                'avatar3' => 'dimensions:min_width=1011,min_height=370 | image',
                'avatar4' => 'dimensions:min_width=1011,min_height=370 | image',
                'avatar5' => 'dimensions:min_width=1011,min_height=370 | image',

            ]);

        DB::table('sports')->where('sports_id', 3)->update(['title' => $request->title]);
        DB::table('sports')->where('sports_id', 3)->update(['body' => $request->body]);

        $data = DB::table('sports')->where('sports_id', 3)->first();  //get the old datas for replacing
        
        if($request->hasFile('avatar1')){
        	
            $avatar = $request->file('avatar1');
            $filename = time() . '1.' . $avatar->getClientOriginalExtension();
            Image::make($avatar)->resize(1011, 370)->save(public_path('/uploads/sports/volleyball/' . $filename));
            DB::table('sports')->where('sports_id', 3)->update(['avatar1' => $filename]);
            unlink(public_path('uploads/sports/volleyball/') . $data->avatar1); //removing old image
         }
         if($request->hasFile('avatar2')){
         	
            $avatar = $request->file('avatar2');
            $filename = time() . '2.' . $avatar->getClientOriginalExtension();
            Image::make($avatar)->resize(1011, 370)->save(public_path('/uploads/sports/volleyball/' . $filename));
            DB::table('sports')->where('sports_id', 3)->update(['avatar2' => $filename]);
            unlink(public_path('uploads/sports/volleyball/') . $data->avatar2); //removing old image
         }
         if($request->hasFile('avatar3')){
         	
            $avatar = $request->file('avatar3');
            $filename = time() . '3.' . $avatar->getClientOriginalExtension();
            Image::make($avatar)->resize(1011, 370)->save(public_path('/uploads/sports/volleyball/' . $filename));
            DB::table('sports')->where('sports_id', 3)->update(['avatar3' => $filename]);
            unlink(public_path('uploads/sports/volleyball/') . $data->avatar3); //removing old image
         }
         if($request->hasFile('avatar4')){
         	
            $avatar = $request->file('avatar4');
            $filename = time() . '4.' . $avatar->getClientOriginalExtension();
            Image::make($avatar)->resize(1011, 370)->save(public_path('/uploads/sports/volleyball/' . $filename));
            DB::table('sports')->where('sports_id', 3)->update(['avatar4' => $filename]);
            unlink(public_path('uploads/sports/volleyball/') . $data->avatar4); //removing old image
         }
         if($request->hasFile('avatar5')){
            $avatar = $request->file('avatar5');
            $filename = time() . '5.' . $avatar->getClientOriginalExtension();
            Image::make($avatar)->resize(1011, 370)->save(public_path('/uploads/sports/volleyball/' . $filename));
            DB::table('sports')->where('sports_id', 3)->update(['avatar5' => $filename]);
            unlink(public_path('uploads/sports/volleyball/') . $data->avatar5); //removing old image
         }

         return redirect()->back()->with('success', 'Updated Successfully.');
  

    }
}
