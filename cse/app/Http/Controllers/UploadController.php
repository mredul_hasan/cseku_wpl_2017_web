<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Campusnews;
use App\Campusevents;
use Image;
class UploadController extends Controller
{
    public function news(){
    	return view('upload.news');
    }

    public function addnews(Request $request, Campusnews $news){
        //validation
        $this->validate($request, [
                'title' => 'required | max:30',
                'heading' => 'required | max:80',
                'body' => 'required',
                'avatar' => 'required | dimensions:min_width=1050,min_height=500 | image',

            ]);



    	 $newNews = new Campusnews;
    	 $newNews->body = $request->body;
    	 $newNews->title = $request->title;
    	 $newNews->heading = $request->heading;
         
    	 //image save part
         if($request->hasFile('avatar')){
            $avatar = $request->file('avatar');
            $filename = time() . '.' . $avatar->getClientOriginalExtension();
            Image::make($avatar)->resize(1011, 370)->save(public_path('/uploads/avatars/' . $filename));
            $newNews->avatar = $filename;
         }

         $news->newss()->save($newNews);
    	
        return redirect()->back()->with('success', 'News Added Successfully.');
    }

    public function events(){
        return view('upload.events');
    }


    public function addevents(Request $request, Campusevents $events){
        //validation
        $this->validate($request, [
                'title' => 'required | max:30',
                'heading' => 'required | max:80',
                'body' => 'required',
                'starting_date' => 'required',
                'starting_time' => 'required',
                'ending_date' => 'required',
                'ending_time' => 'required',
                'avatar' => 'required | dimensions:min_width=1050,min_height=500 | image',

            ]);



         $newEvent = new Campusevents;
         $newEvent->body = $request->body;
         $newEvent->title = $request->title;
         $newEvent->heading = $request->heading;
         $newEvent->start_date = $request->starting_date;
         $newEvent->start_time = $request->starting_time;
         $newEvent->end_date = $request->ending_date;
         $newEvent->end_time = $request->ending_time;
         
         //image save part
         if($request->hasFile('avatar')){
            $avatar = $request->file('avatar');
            $filename = time() . '.' . $avatar->getClientOriginalExtension();
            Image::make($avatar)->resize(1011, 370)->save(public_path('/uploads/avatars/' . $filename));
            $newEvent->avatar = $filename;
         }

         $events->eventss()->save($newEvent);
        
        return redirect()->back()->with('success', 'Event Added Successfully.');
    }
}
