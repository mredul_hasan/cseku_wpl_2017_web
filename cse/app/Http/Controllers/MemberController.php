<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Members;
use Image;
use DB;

class MemberController extends Controller
{
    public function go(){

    	return view('FacultyPeople.addMember');
    }

    public function addMember(Request $request, Members $member){

    	  //validation
        $this->validate($request, [
                'name' => 'required',
                'designation' => 'required',
                'contact' => 'size:11',
                //'link' => 'active_url',
                'avatar' => 'dimensions:min_width=300,min_height=300 | image',

            ]);


    	 $newMember = new Members;
    	 $newMember->name = $request->name;
    	 $newMember->designation = $request->designation;
    	 
    	 $deg1 = 'Lecturer';
    	 $deg2 = 'Assistant Professor';
    	 $deg3 = 'Associate Professor';
    	 $deg4 = 'Professor';
    	 $deg5 = 'Professor (Head)';

    	 if($request->designation == $deg1)
    	 	$newMember->rank = 1;
    	 else if($request->designation == $deg2)
    	 	$newMember->rank = 2;
    	 else if($request->designation == $deg3)
    	 	$newMember->rank = 3;
    	 else if($request->designation == $deg4)
    	 	$newMember->rank = 4;
    	 else if($request->designation == $deg5)
    	 	$newMember->rank = 5;

    	 if($request->link)
    	    $newMember->link = $request->link;
    	 if($request->contact)
    	 	$newMember->contact = $request->contact;
         
    	 //image save part
         if($request->hasFile('avatar')){
            $avatar = $request->file('avatar');
            $filename = time() . '.' . $avatar->getClientOriginalExtension();
            Image::make($avatar)->resize(300, 300)->save(public_path('/uploads/facultyPeople/members/' . $filename));
            $newMember->avatar = $filename;
         }
         
        $newMember->members()->save($newMember);
    	
    	return redirect()->back()->with('success', 'Member Added Successfully.');
    }


    public function goUP(){

        $members = Members ::orderBy('rank', 'desc')->get();
        return view('FacultyPeople.memberUpdateList', compact('members'));
    }

    public function goUPbyID($id){

        $member = DB::table('members')->where('members_id', $id)->first();
        return view('FacultyPeople.updateMemberById', compact('member'));
    }

    public function updateByID(Request $request, $id){

          //validation
        $this->validate($request, [
                'name' => 'required',
                'designation' => 'required',
                'contact' => 'required',
                'link' => 'required',
                'avatar' => 'dimensions:min_width=300,min_height=300 | image',

            ]);

         $deg1 = 'Lecturer';
         $deg2 = 'Assistant Professor';
         $deg3 = 'Associate Professor';
         $deg4 = 'Professor';
         $deg5 = 'Professor (Head)';
         $value = 0;

         if($request->designation == $deg1)
            $value = 1;
         else if($request->designation == $deg2)
            $value = 2;
         else if($request->designation == $deg3)
            $value = 3;
         else if($request->designation == $deg4)
            $value = 4;
         else if($request->designation == $deg5)
            $value = 5;


        DB::table('members')->where('members_id', $id)->update([
            'name' => $request->name,
            'designation' => $request->designation,
            'link' => $request->link,
            'contact' => $request->contact,
            'rank' => $value
            ]);


        $data = DB::table('members')->where('members_id', $id)->first();  //get the old datas for replacing
        
        if($request->hasFile('avatar')){
            
            $avatar = $request->file('avatar');
            $filename = time() . '.' . $avatar->getClientOriginalExtension();
            Image::make($avatar)->resize(300, 300)->save(public_path('/uploads/facultyPeople/members/' . $filename));
            DB::table('members')->where('members_id', $id)->update(['avatar' => $filename]);
            unlink(public_path('uploads/facultyPeople/members/') . $data->avatar); //removing old image
         }


        return redirect()->back()->with('success', 'Updated Successfully.');

    }
}
