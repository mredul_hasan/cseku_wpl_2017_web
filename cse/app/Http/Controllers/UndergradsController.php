<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use DB;
use Image;
use Input;

class UndergradsController extends Controller
{
    public function go(){
    	$data = DB::table('academics')->where('academics_id', 1)->first();
    	return view('academics.undergrads', compact('data'));
    }

    public function goUP(){
    	$data = DB::table('academics')->where('academics_id', 1)->first();
    	return view('academics.updateUndergrads', compact('data'));
    }

    public function update(Request $request){
    	//validation
        $this->validate($request, [
        		'title' => 'required',
        		'body' => 'required',
                'avatar' => 'dimensions:min_width=1011,min_height=370 | image',
                'file' => 'file',
            ]);

        DB::table('academics')->where('academics_id', 1)->update(['title' => $request->title]);
        DB::table('academics')->where('academics_id', 1)->update(['body' => $request->body]);

        $data = DB::table('academics')->where('academics_id', 1)->first();  //get the old datas for replacing
        
        if($request->hasFile('avatar')){
        	
            $avatar = $request->file('avatar');
            $filename = time() . '.' . $avatar->getClientOriginalExtension();
            Image::make($avatar)->resize(1011, 370)->save(public_path('/uploads/academics/' . $filename));
            DB::table('academics')->where('academics_id', 1)->update(['avatar' => $filename]);
            unlink(public_path('uploads/academics/') . $data->avatar); //removing old image
         }

        if($request->hasFile('syllabus')){
         	$file = $request->file('syllabus');
            $destinationPath = 'uploads/academics'; // upload path
	        $fileName = uniqid() . $file->getClientOriginalName(); // renameing file
	        $file->move($destinationPath, $fileName); // uploading file to given path
            DB::table('academics')->where('academics_id', 1)->update(['syllabus' => $fileName]);
            unlink(public_path('uploads/academics/') . $data->syllabus); //removing old file
        }


         return redirect()->back()->with('success', 'Updated Successfully.');
    }

}
