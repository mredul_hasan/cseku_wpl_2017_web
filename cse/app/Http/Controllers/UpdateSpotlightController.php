<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use DB;
use Image;

class UpdateSpotlightController extends Controller
{
    public function go(){
    	$spot = DB::table('campusspotlight')->where('campusspotlight_id', 1)->first();
    	return view('spotlight.updateSpotlight', compact('spot'));
    }

    public function updateSpotlight(Request $request){
    	 //validation
        $this->validate($request, [
        		'title' => 'required',
        		'heading' => 'required',
        		'body' => 'required',
        		'avatarCover' => 'dimensions:min_width=1050,min_height=500 | image',
                'avatar1' => 'dimensions:min_width=1050,min_height=500 | image',
                'avatar2' => 'dimensions:min_width=1050,min_height=500 | image',
                'avatar3' => 'dimensions:min_width=1050,min_height=500 | image',
                'avatar4' => 'dimensions:min_width=1050,min_height=500 | image',
                'avatar5' => 'dimensions:min_width=1050,min_height=500 | image',

            ]);


        DB::table('campusspotlight')->where('campusspotlight_id', 1)->update(['title' => $request->title]);
        DB::table('campusspotlight')->where('campusspotlight_id', 1)->update(['heading' => $request->heading]);
        DB::table('campusspotlight')->where('campusspotlight_id', 1)->update(['body' => $request->body]);


        $spot = DB::table('campusspotlight')->where('campusspotlight_id', 1)->first();  //get the old datas for replacing
        
        if($request->hasFile('avatarCover')){
        	
            $avatar = $request->file('avatarCover');
            $filename = time() . '0.' . $avatar->getClientOriginalExtension();
            Image::make($avatar)->resize(1011, 370)->save(public_path('/uploads/spotlight/' . $filename));
            DB::table('campusspotlight')->where('campusspotlight_id', 1)->update(['avatarCover' => $filename]);
            unlink(public_path('uploads/spotlight/') . $spot->avatarCover); //removing old image
         }
          if($request->hasFile('avatar1')){
        	
            $avatar = $request->file('avatar1');
            $filename = time() . '1.' . $avatar->getClientOriginalExtension();
            Image::make($avatar)->resize(1011, 370)->save(public_path('/uploads/spotlight/' . $filename));
            DB::table('campusspotlight')->where('campusspotlight_id', 1)->update(['avatar1' => $filename]);
            unlink(public_path('uploads/spotlight/') . $spot->avatar1); //removing old image
         }
          if($request->hasFile('avatar2')){
        	
            $avatar = $request->file('avatar2');
            $filename = time() . '2.' . $avatar->getClientOriginalExtension();
            Image::make($avatar)->resize(1011, 370)->save(public_path('/uploads/spotlight/' . $filename));
            DB::table('campusspotlight')->where('campusspotlight_id', 1)->update(['avatar2' => $filename]);
            unlink(public_path('uploads/spotlight/') . $spot->avatar2); //removing old image
         }
          if($request->hasFile('avatar3')){
        	
            $avatar = $request->file('avatar3');
            $filename = time() . '3.' . $avatar->getClientOriginalExtension();
            Image::make($avatar)->resize(1011, 370)->save(public_path('/uploads/spotlight/' . $filename));
            DB::table('campusspotlight')->where('campusspotlight_id', 1)->update(['avatar3' => $filename]);
            unlink(public_path('uploads/spotlight/') . $spot->avatar3); //removing old image
         }
          if($request->hasFile('avatar4')){
        	
            $avatar = $request->file('avatar4');
            $filename = time() . '4.' . $avatar->getClientOriginalExtension();
            Image::make($avatar)->resize(1011, 370)->save(public_path('/uploads/spotlight/' . $filename));
            DB::table('campusspotlight')->where('campusspotlight_id', 1)->update(['avatar4' => $filename]);
            unlink(public_path('uploads/spotlight/') . $spot->avatar4); //removing old image
         }
          if($request->hasFile('avatar5')){
        	
            $avatar = $request->file('avatar5');
            $filename = time() . '5.' . $avatar->getClientOriginalExtension();
            Image::make($avatar)->resize(1011, 370)->save(public_path('/uploads/spotlight/' . $filename));
            DB::table('campusspotlight')->where('campusspotlight_id', 1)->update(['avatar5' => $filename]);
            unlink(public_path('uploads/spotlight/') . $spot->avatar5); //removing old image
         }


        return redirect()->back()->with('success', 'Updated Successfully.');
    }
}
