<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use DB;
use Image;

class StdAlmController extends Controller
{
    public function students(){
    	$data = DB::table('stdalmpage')->where('stdalmpage_id', 1)->first();
    	return view('std&alm.Students', compact('data'));
    }

    public function alumni(){
    	$data = DB::table('stdalmpage')->where('stdalmpage_id', 2)->first();
    	return view('std&alm.Alumni', compact('data'));
    }

    public function studentsUP(){
    	$data = DB::table('stdalmpage')->where('stdalmpage_id', 1)->first();
    	return view('std&alm.updateStudents', compact('data'));
    }

    public function updateStudents(Request $request){
    	 //validation
        $this->validate($request, [
        		'title' => 'required',
        		'body' => 'required',
                'avatar1' => 'dimensions:min_width=1050,min_height=500 | image',
                'avatar2' => 'dimensions:min_width=1050,min_height=500 | image',
                'avatar3' => 'dimensions:min_width=1050,min_height=500 | image',
                'avatar4' => 'dimensions:min_width=1050,min_height=500 | image',
                'avatar5' => 'dimensions:min_width=1050,min_height=500 | image',

            ]);

        DB::table('stdalmpage')->where('stdalmpage_id', 1)->update(['title' => $request->title]);
        DB::table('stdalmpage')->where('stdalmpage_id', 1)->update(['body' => $request->body]);

        $data = DB::table('stdalmpage')->where('stdalmpage_id', 1)->first();  //get the old datas for replacing
        
        if($request->hasFile('avatar1')){
        	
            $avatar = $request->file('avatar1');
            $filename = time() . '1.' . $avatar->getClientOriginalExtension();
            Image::make($avatar)->resize(1011, 370)->save(public_path('/uploads/StdAlm/students/' . $filename));
            DB::table('stdalmpage')->where('stdalmpage_id', 1)->update(['avatar1' => $filename]);
            unlink(public_path('uploads/StdAlm/students/') . $data->avatar1); //removing old image
         }
         if($request->hasFile('avatar2')){
         	
            $avatar = $request->file('avatar2');
            $filename = time() . '2.' . $avatar->getClientOriginalExtension();
            Image::make($avatar)->resize(1011, 370)->save(public_path('/uploads/StdAlm/students/' . $filename));
            DB::table('stdalmpage')->where('stdalmpage_id', 1)->update(['avatar2' => $filename]);
            unlink(public_path('uploads/StdAlm/students/') . $data->avatar2); //removing old image
         }
         if($request->hasFile('avatar3')){
         	
            $avatar = $request->file('avatar3');
            $filename = time() . '3.' . $avatar->getClientOriginalExtension();
            Image::make($avatar)->resize(1011, 370)->save(public_path('/uploads/StdAlm/students/' . $filename));
            DB::table('stdalmpage')->where('stdalmpage_id', 1)->update(['avatar3' => $filename]);
            unlink(public_path('uploads/StdAlm/students/') . $data->avatar3); //removing old image
         }
         if($request->hasFile('avatar4')){
         	
            $avatar = $request->file('avatar4');
            $filename = time() . '4.' . $avatar->getClientOriginalExtension();
            Image::make($avatar)->resize(1011, 370)->save(public_path('/uploads/StdAlm/students/' . $filename));
            DB::table('stdalmpage')->where('stdalmpage_id', 1)->update(['avatar4' => $filename]);
            unlink(public_path('uploads/StdAlm/students/') . $data->avatar4); //removing old image
         }
         if($request->hasFile('avatar5')){
            $avatar = $request->file('avatar5');
            $filename = time() . '5.' . $avatar->getClientOriginalExtension();
            Image::make($avatar)->resize(1011, 370)->save(public_path('/uploads/StdAlm/students/' . $filename));
            DB::table('stdalmpage')->where('stdalmpage_id', 1)->update(['avatar5' => $filename]);
            unlink(public_path('uploads/StdAlm/students/') . $data->avatar5); //removing old image
         }

         return redirect()->back()->with('success', 'Updated Successfully.');
  

    }

    public function alumniUP(){
    	$data = DB::table('stdalmpage')->where('stdalmpage_id', 2)->first();
    	return view('std&alm.updateAlumni', compact('data'));
    }

        public function updateAlumni(Request $request){
    	 //validation
        $this->validate($request, [
        		'title' => 'required',
        		'body' => 'required',
                'avatar1' => 'dimensions:min_width=1050,min_height=500 | image',
                'avatar2' => 'dimensions:min_width=1050,min_height=500 | image',
                'avatar3' => 'dimensions:min_width=1050,min_height=500 | image',
                'avatar4' => 'dimensions:min_width=1050,min_height=500 | image',
                'avatar5' => 'dimensions:min_width=1050,min_height=500 | image',

            ]);

        DB::table('stdalmpage')->where('stdalmpage_id', 2)->update(['title' => $request->title]);
        DB::table('stdalmpage')->where('stdalmpage_id', 2)->update(['body' => $request->body]);

        $data = DB::table('stdalmpage')->where('stdalmpage_id', 2)->first();  //get the old datas for replacing
        
        if($request->hasFile('avatar1')){
        	
            $avatar = $request->file('avatar1');
            $filename = time() . '1.' . $avatar->getClientOriginalExtension();
            Image::make($avatar)->resize(1011, 370)->save(public_path('/uploads/StdAlm/alumni/' . $filename));
            DB::table('stdalmpage')->where('stdalmpage_id', 2)->update(['avatar1' => $filename]);
            unlink(public_path('uploads/StdAlm/alumni/') . $data->avatar1); //removing old image
         }
         if($request->hasFile('avatar2')){
         	
            $avatar = $request->file('avatar2');
            $filename = time() . '2.' . $avatar->getClientOriginalExtension();
            Image::make($avatar)->resize(1011, 370)->save(public_path('/uploads/StdAlm/alumni/' . $filename));
            DB::table('stdalmpage')->where('stdalmpage_id', 2)->update(['avatar2' => $filename]);
            unlink(public_path('uploads/StdAlm/alumni/') . $data->avatar2); //removing old image
         }
         if($request->hasFile('avatar3')){
         	
            $avatar = $request->file('avatar3');
            $filename = time() . '3.' . $avatar->getClientOriginalExtension();
            Image::make($avatar)->resize(1011, 370)->save(public_path('/uploads/StdAlm/alumni/' . $filename));
            DB::table('stdalmpage')->where('stdalmpage_id', 2)->update(['avatar3' => $filename]);
            unlink(public_path('uploads/StdAlm/alumni/') . $data->avatar3); //removing old image
         }
         if($request->hasFile('avatar4')){
         	
            $avatar = $request->file('avatar4');
            $filename = time() . '4.' . $avatar->getClientOriginalExtension();
            Image::make($avatar)->resize(1011, 370)->save(public_path('/uploads/StdAlm/alumni/' . $filename));
            DB::table('stdalmpage')->where('stdalmpage_id', 2)->update(['avatar4' => $filename]);
            unlink(public_path('uploads/StdAlm/alumni/') . $data->avatar4); //removing old image
         }
         if($request->hasFile('avatar5')){
            $avatar = $request->file('avatar5');
            $filename = time() . '5.' . $avatar->getClientOriginalExtension();
            Image::make($avatar)->resize(1011, 370)->save(public_path('/uploads/StdAlm/alumni/' . $filename));
            DB::table('stdalmpage')->where('stdalmpage_id', 2)->update(['avatar5' => $filename]);
            unlink(public_path('uploads/StdAlm/alumni/') . $data->avatar5); //removing old image
         }

        return redirect()->back()->with('success', 'Updated Successfully.');
  

    }
}
