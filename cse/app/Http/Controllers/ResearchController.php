<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Research;
use App\Campusnews;
use App\Campusevents;
use Carbon;
use DB;

class ResearchController extends Controller
{
    
    public function go(){
    	$researches = Research ::orderBy('research_id', 'desc')->get();

        $news = Campusnews ::orderBy('campusnews_id', 'desc')->get();
        $events = Campusevents ::orderBy('campusevents_id', 'desc')->get();

        $days = array();
        $months = array();
        foreach ($events as $event) {
            $dt = $event->start_date;
            $d = Carbon::parse($dt)->format('d');
            $m = Carbon::parse($dt)->format('m');

            array_push($days, $d);
            if($m == 1)
                array_push($months, 'Jan');
            else if ($m == 2) {
                array_push($months, 'Feb');
            }
            else if ($m == 3) {
                array_push($months, 'Mar');
            }
            else if ($m == 4) {
                array_push($months, 'Apr');
            }
            else if ($m == 5) {
                array_push($months, 'May');
            }
            else if ($m == 6) {
                array_push($months, 'Jun');
            }
            else if ($m == 7) {
                array_push($months, 'Jul');
            }
            else if ($m == 8) {
                array_push($months, 'Aug');
            }
            else if ($m == 9) {
                array_push($months, 'Sep');
            }
            else if ($m == 10) {
                array_push($months, 'Oct');
            }
            else if ($m == 11) {
                array_push($months, 'Nov');
             }
            else
                array_push($months, 'Dec');

            
        }


    	return view('research.Research', compact('researches', 'news', 'events', 'months', 'days'));
    }

     public function fullList(){
    	$researches = Research ::orderBy('research_id', 'desc')->get();

        $news = Campusnews ::orderBy('campusnews_id', 'desc')->get();
        $events = Campusevents ::orderBy('campusevents_id', 'desc')->get();

        $days = array();
        $months = array();
        foreach ($events as $event) {
            $dt = $event->start_date;
            $d = Carbon::parse($dt)->format('d');
            $m = Carbon::parse($dt)->format('m');

            array_push($days, $d);
            if($m == 1)
                array_push($months, 'Jan');
            else if ($m == 2) {
                array_push($months, 'Feb');
            }
            else if ($m == 3) {
                array_push($months, 'Mar');
            }
            else if ($m == 4) {
                array_push($months, 'Apr');
            }
            else if ($m == 5) {
                array_push($months, 'May');
            }
            else if ($m == 6) {
                array_push($months, 'Jun');
            }
            else if ($m == 7) {
                array_push($months, 'Jul');
            }
            else if ($m == 8) {
                array_push($months, 'Aug');
            }
            else if ($m == 9) {
                array_push($months, 'Sep');
            }
            else if ($m == 10) {
                array_push($months, 'Oct');
            }
            else if ($m == 11) {
                array_push($months, 'Nov');
             }
            else
                array_push($months, 'Dec');

            
        }


    	return view('research.ResearchList', compact('researches', 'news', 'events', 'months', 'days'));
    }

    public function goUP(){
    	return view('research.addResearch');
    }

    public function add(Request $request, Research $research){
    	  //validation
       	 $this->validate($request, [
                'title' => 'required',
                'category' => 'required',
                'author' => 'required',
                //'link' => 'active_url',

            ]);


       	  $newResearch = new Research;
    	 $newResearch->title = $request->title;
    	 $newResearch->category = $request->category;
    	 $newResearch->author = $request->author;
    	 if($request->link)
    	    $newResearch->link = $request->link;

         $research->researches()->save($newResearch);
    	

        return redirect()->back()->with('success', 'Research Added Successfully.');
    }
}
