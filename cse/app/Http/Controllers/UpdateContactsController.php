<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use DB;

class UpdateContactsController extends Controller
{
    public function go($id){
    	$contact = DB::table('contacts')->where('contacts_id', $id)->first();
    	return view('UpdateContacts', compact('contact'));
    }

    public function updateContacts(Request $request, $id){
    	 //validation
        $this->validate($request, [
        		'title' => 'required',
        		'heading' => 'required',
             	'call' => 'required | size:11',
             	'fax' => 'required | size:9',
             	'email' => 'required | email'
            ]);

        DB::table('contacts')->where('contacts_id', $id)->update([
        	'title' => $request->title,
        	'heading' => $request->heading,
        	'call' => $request->call,
        	'fax' => $request->fax,
        	'email' => $request->email
        	]);

        return redirect()->back()->with('success', 'Updated Successfully.');

    }
}
