<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use DB;

class SpotlightController extends Controller
{
    public function go(){
    	$spot = DB::table('campusspotlight')->where('campusspotlight_id', 1)->first();
    	return view('spotlight.spotlight', compact('spot'));
    }
}
