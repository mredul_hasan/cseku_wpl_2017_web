<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Staffs;
use Image;
use DB;

class StaffController extends Controller
{
    public function go(){
    	return view('FacultyPeople.addStaff');
    }

    public function addStaff(Request $request, Staffs $staff){

    	  //validation
        $this->validate($request, [
                'name' => 'required',
                'designation' => 'required',
                'contact' => 'size:11',
                //'link' => 'active_url',
                'avatar' => 'dimensions:min_width=300,min_height=300 | image',

            ]);


    	 $newStaff = new Staffs;
    	 $newStaff->name = $request->name;
    	 $newStaff->designation = $request->designation;
    	 if($request->link)
    	    $newStaff->link = $request->link;
    	 if($request->contact)
    	 	$newStaff->contact = $request->contact;
         
    	 //image save part
         if($request->hasFile('avatar')){
            $avatar = $request->file('avatar');
            $filename = time() . '.' . $avatar->getClientOriginalExtension();
            Image::make($avatar)->resize(300, 300)->save(public_path('/uploads/facultyPeople/staffs/' . $filename));
            $newStaff->avatar = $filename;
         }

         $staff->staffs()->save($newStaff);
    	
        return redirect()->back()->with('success', 'Staff Added Successfully.');
    }


    public function goUP(){

        $staffs = Staffs ::all();
        return view('FacultyPeople.staffUpdateList', compact('staffs'));
    }

    public function goUPbyID($id){

        $staff = DB::table('staffs')->where('staffs_id', $id)->first();
        return view('FacultyPeople.updateStaffById', compact('staff'));
    }

    public function updateByID(Request $request, $id){

          //validation
        $this->validate($request, [
                'name' => 'required',
                'designation' => 'required',
                'contact' => 'required',
                'link' => 'required',
                'avatar' => 'dimensions:min_width=300,min_height=300 | image',

            ]);


        DB::table('staffs')->where('staffs_id', $id)->update([
            'name' => $request->name,
            'designation' => $request->designation,
            'link' => $request->link,
            'contact' => $request->contact,
            ]);


        $data = DB::table('staffs')->where('staffs_id', $id)->first();  //get the old datas for replacing
        
        if($request->hasFile('avatar')){
            
            $avatar = $request->file('avatar');
            $filename = time() . '.' . $avatar->getClientOriginalExtension();
            Image::make($avatar)->resize(300, 300)->save(public_path('/uploads/facultyPeople/staffs/' . $filename));
            DB::table('staffs')->where('staffs_id', $id)->update(['avatar' => $filename]);
            unlink(public_path('uploads/facultyPeople/staffs/') . $data->avatar); //removing old image
         }


        return redirect()->back()->with('success', 'Updated Successfully.');

    }
}
