<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>

	<form enctype="multipart/form-data" method="POST" action="{{$member->members_id}}">
	{{ method_field('PATCH') }}
	{{csrf_field()}}
	<div>
		<label>Member Name</label><br>
		<textarea name="name">{{$member->name}}</textarea><br>
		<label>Member Designation</label><br>

		<select name="designation" id="">
		<option selected="selected">{{$member->designation}}</option>
		<option value="Professor (Head)">Professor (Head)</option>
		<option value="Professor">Professor</option>
		<option value="Associate Professor">Associate Professor</option>
		<option value="Assistant Professor">Assistant Professor</option>
		<option value="Lecturer">Lecturer</option>
		</select>

		
		<br><br><label>Web Link</label><br>
		<textarea name="link">{{$member->link}}</textarea><br>
		<label>Contact</label><br>
		<textarea name="contact">{{$member->contact}}</textarea><br>
		<label>Pick a Image (least dimension of 300x300)</label><br>
		<input type="file" name="avatar"><br><br>





	</div>

	<div>
		<button type="submit">Update Member</button>
	</div>
	</form>

	@if(Session::has('success'))
		<h3 style="color: green">{{Session::get('success')}}</h3>
	@endif

	@if(count($errors))
		<ul>
			@foreach($errors->all() as $error)
				<li>{{$error}}</li>
			@endforeach
		</ul>
	@endif

</body>
</html>