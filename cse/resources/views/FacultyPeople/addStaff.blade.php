<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>

	<form enctype="multipart/form-data" method="POST" action="addStaff">
	{{csrf_field()}}
	<div>
		<label>Staff Name</label><br>
		<textarea name="name">{{old('name')}}</textarea><br>
		<label>Staff Designation</label><br>
		<textarea name="designation">{{old('designation')}}</textarea><br>
		<label>Web Link (www.example.com)</label><br>
		<textarea name="link">{{old('link')}}</textarea><br>
		<label>Contact</label><br>
		<textarea name="contact">{{old('contact')}}</textarea><br>
		<label>Pick a Image (least dimension of 300x300)</label><br>
		<input type="file" name="avatar"><br><br>
	</div>

	<div>
		<button type="submit">Add Staff</button>
	</div>
	</form>

	@if(Session::has('success'))
		<h3 style="color: green">{{Session::get('success')}}</h3>
	@endif

	@if(count($errors))
		<ul>
			@foreach($errors->all() as $error)
				<li>{{$error}}</li>
			@endforeach
		</ul>
	@endif

</body>
</html>