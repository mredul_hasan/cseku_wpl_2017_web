<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>

	<form enctype="multipart/form-data" method="POST" action="{{$staff->staffs_id}}">
	{{ method_field('PATCH') }}
	{{csrf_field()}}
	<div>
		<label>Staff Name</label><br>
		<textarea name="name">{{$staff->name}}</textarea><br>
		<label>Staff Designation</label><br>
		<textarea name="designation">{{$staff->designation}}</textarea><br>
		<label>Web Link (www.example.com)</label><br>
		<textarea name="link">{{$staff->link}}</textarea><br>
		<label>Contact</label><br>
		<textarea name="contact">{{$staff->contact}}</textarea><br>
		<label>Pick a new Image (least dimension of 300x300)</label><br>
		<input type="file" name="avatar"><br><br>
	</div>

	<div>
		<button type="submit">Update Staff</button>
	</div>
	</form>

	@if(Session::has('success'))
		<h3 style="color: green">{{Session::get('success')}}</h3>
	@endif

	@if(count($errors))
		<ul>
			@foreach($errors->all() as $error)
				<li>{{$error}}</li>
			@endforeach
		</ul>
	@endif

</body>
</html>