<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>

	<form enctype="multipart/form-data" method="POST" action="updateAlumni">
	{{ method_field('PATCH') }}
	{{ csrf_field() }}
	<div>
		<label>Edit the Title</label><br>
		<textarea name="title" style="width: 60%; height: 50%;">{{$data->title}}</textarea><br>
		<label>Edit the Description</label><br>
		<textarea name="body" style="width: 60%; height: 50%;">{{$data->body}}</textarea><br>
		<label>Update Images</label><br>
		
		<p>Choose images with a least width of 1050px & a least height of 500px</p>
		<label>Choose Picture 1</label><br>
		<input type="file" name="avatar1"><br><br>
		<label>Choose Picture 2</label><br>
		<input type="file" name="avatar2"><br><br>
		<label>Choose Picture 3</label><br>
		<input type="file" name="avatar3"><br><br>
		<label>Choose Picture 4</label><br>
		<input type="file" name="avatar4"><br><br>
		<label>Choose Picture 5</label><br>
		<input type="file" name="avatar5"><br><br>
	</div>

	<div>
		<button type="submit">Save Changes</button>
	</div>
	</form>

	@if(Session::has('success'))
		<h3 style="color: green">{{Session::get('success')}}</h3>
	@endif

	@if(count($errors))
		<ul>
			@foreach($errors->all() as $error)
				<li>{{$error}}</li>
			@endforeach
		</ul>
	@endif

</body>
</html>