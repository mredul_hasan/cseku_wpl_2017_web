@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">

                <ul>
                    <li style="background:none; padding-right:0px;"><a href="uploadNews">Upload New News</a></li>
                    <li style="background:none; padding-right:0px;"><a href="uploadEvents">Upload Event News</a></li>
                    <li style="background:none; padding-right:0px;"><a href="updateSpotlight">Update Campus Spotlight</a></li>
                    <li style="background:none; padding-right:0px;"><a href="addResearch">Add New Research</a></li>

                    <li style="background:none; padding-right:0px;">Update Students & Alumni Page
                        <ul>
                            <li><a href="updateStudents">Students</a></li>
                            <li><a href="updateAlumni">Alumni</a></li>
                        </ul>
                    </li>

                    <li style="background:none; padding-right:0px;">Faculty People
                        <ul>
                            <li><a href="addMember">Add New Faculty Member</a></li>
                            <li><a href="addStaff">Add New Faculty Staff</a></li>
                            <li><a href="updateMember">Update Faculty Members</a></li>
                            <li><a href="updateStaff">Update Faculty Staffs</a></li>
                        </ul>
                    </li>
                    
                    <li style="background:none; padding-right:0px;">Update Academics
                        <ul>
                            <li><a href="updateUndergrad">Undergraduate Programs</a></li>
                            <li><a href="updateGrad">Graduate Programs</a></li>
                        </ul>
                    </li>

                    <li style="background:none; padding-right:0px;">Update Sport News
                        <ul>
                            <li><a href="updateFootball">Football</a></li>
                            <li><a href="updateCricket">Cricket</a></li>
                            <li><a href="updateVolleyball">Volleyball</a></li>
                        </ul>
                    </li>
                    <li style="background:none; padding-right:0px;">Update Contacts
                        <ul>
                            <li><a href="updateContacts/1">General Information</a></li>
                            <li><a href="updateContacts/2">Admission Information</a></li>
                            <li><a href="updateContacts/3">University Information</a></li>
                            <li><a href="updateContacts/4">Press Contact Information</a></li>
                        </ul>
                    </li>
                </ul>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection