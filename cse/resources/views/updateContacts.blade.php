<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>

	<form enctype="multipart/form-data" method="POST" action="">
	{{ method_field('PATCH') }}
	{{ csrf_field() }}
	<div>
		<label>Edit the Title</label><br>
		<textarea name="title" style="width: 60%; height: 50%;">{{$contact->title}}</textarea><br>
		<label>Edit the Heading</label><br>
		<textarea name="heading" style="width: 60%; height: 50%;">{{$contact->heading}}</textarea><br>
		<label>Update Contacts</label><br>
		<textarea name="call" style="width: 60%; height: 50%;">{{$contact->call}}</textarea><br>
		<label>Update Fax Number</label><br>
		<textarea name="fax" style="width: 60%; height: 50%;">{{$contact->fax}}</textarea><br>
		<label>Update Email</label><br>
		<textarea name="email" style="width: 60%; height: 50%;">{{$contact->email}}</textarea><br>
	</div>

	<div>
		<button type="submit">Save Changes</button>
	</div>
	</form>

	@if(Session::has('success'))
		<h3 style="color: green">{{Session::get('success')}}</h3>
	@endif

	@if(count($errors))
		<ul>
			@foreach($errors->all() as $error)
				<li>{{$error}}</li>
			@endforeach
		</ul>
	@endif

</body>
</html>