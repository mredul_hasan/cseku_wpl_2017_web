<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>

	<form enctype="multipart/form-data" method="POST" action="updateGrad">
	{{ method_field('PATCH') }}
	{{ csrf_field() }}
	<div>
		<label>Edit the Title</label><br>
		<textarea name="title" style="width: 60%; height: 50%;">{{$data->title}}</textarea><br>
		<label>Edit the Description</label><br>
		<textarea name="body" style="width: 60%; height: 50%;">{{$data->body}}</textarea><br>
		<label>Update Image</label><br>
		
		<p>Choose an image with a least width of 1050px & a least height of 500px</p>
		<label>Choose Picture</label><br>
		<input type="file" name="avatar"><br><br>

		<label>Update Syllabus</label><br>
		
		<p>Choose a PDF File</p>
		<label>Choose File</label><br>
		<input type="file" name="syllabus"><br><br>
	</div>

	<div>
		<button type="submit">Save Changes</button>
	</div>
	</form>

	@if(Session::has('success'))
		<h3 style="color: green">{{Session::get('success')}}</h3>
	@endif

	@if(count($errors))
		<ul>
			@foreach($errors->all() as $error)
				<li>{{$error}}</li>
			@endforeach
		</ul>
	@endif

</body>
</html>