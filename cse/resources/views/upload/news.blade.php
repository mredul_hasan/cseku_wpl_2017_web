<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>

	<form enctype="multipart/form-data" method="POST" action="uploadNews">
	{{csrf_field()}}
	<div>
		<label>Give a Title</label><br>
		<textarea name="title">{{old('title')}}</textarea><br>
		<label>Put a Heading</label><br>
		<textarea name="heading">{{old('heading')}}</textarea><br>
		<label>Fill out the Body</label><br>
		<textarea name="body">{{old('body')}}</textarea><br>
		<label>Pick a Image</label><br>
		<input type="file" name="avatar"><br><br>
	</div>

	<div>
		<button type="submit">Add News</button>
	</div>
	</form>

	@if(Session::has('success'))
		<h3 style="color: green">{{Session::get('success')}}</h3>
	@endif

	@if(count($errors))
		<ul>
			@foreach($errors->all() as $error)
				<li>{{$error}}</li>
			@endforeach
		</ul>
	@endif

</body>
</html>