<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>

	<form enctype="multipart/form-data" method="POST" action="addResearch">
	{{csrf_field()}}
	<div>
		<label>Research Title</label><br>
		<textarea name="title">{{old('title')}}</textarea><br>
		<label>Category</label><br>
		<textarea name="category">{{old('category')}}</textarea><br>
		<label>Web Link (www.example.com)</label><br>
		<textarea name="link">{{old('link')}}</textarea><br>
		<label>Author</label><br>
		<textarea name="author">{{old('author')}}</textarea><br>
	</div>

	<div>
		<button type="submit">Add Research</button>
	</div>
	</form>

	@if(Session::has('success'))
		<h3 style="color: green">{{Session::get('success')}}</h3>
	@endif

	@if(count($errors))
		<ul>
			@foreach($errors->all() as $error)
				<li>{{$error}}</li>
			@endforeach
		</ul>
	@endif

</body>
</html>