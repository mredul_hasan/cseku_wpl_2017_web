<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableStdalmpage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stdalmpage', function (Blueprint $table) {
            $table->increments('stdalmpage_id');
            $table->string('title');
            $table->string('avatar1');
            $table->string('avatar2');
            $table->string('avatar3');
            $table->string('avatar4');
            $table->string('avatar5');
            $table->string('body');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stdalmpage');
    }
}
