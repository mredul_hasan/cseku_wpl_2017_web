<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableContacts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
      public function up()
    {
        Schema::create('contacts', function (Blueprint $table) {
            $table->increments('contacts_id');
            $table->string('title');
            $table->string('heading');
            $table->string('email')->default('Not Available');
            $table->string('call')->default('Not Available');
            $table->string('fax')->default('Not Available');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
   public function down()
    {
        Schema::drop('contacts');
    }
}
