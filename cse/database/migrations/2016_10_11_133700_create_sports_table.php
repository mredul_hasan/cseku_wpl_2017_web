<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sports', function (Blueprint $table) {
            $table->increments('sports_id');
            $table->string('title');
            $table->string('avatar1')->default('default1.jng');
            $table->string('avatar2')->default('default2.jng');
            $table->string('avatar3')->default('default3.jng');
            $table->string('avatar4')->default('default4.jng');
            $table->string('avatar5')->default('default5.jpg');
            $table->string('body');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sports');
    }
}
