<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampusspotlightTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campusspotlight', function (Blueprint $table) {
            $table->increments('campusspotlight_id');
            $table->string('title');
            $table->string('heading');
            $table->string('body');
            $table->string('avatarCover')->default('default1.jng');
            $table->string('avatar1')->default('default2.jng');
            $table->string('avatar2')->default('default3.jng');
            $table->string('avatar3')->default('default4.jng');
            $table->string('avatar4')->default('default5.jng');
            $table->string('avatar5')->default('default6.jpg');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campusspotlight');
    }
}
