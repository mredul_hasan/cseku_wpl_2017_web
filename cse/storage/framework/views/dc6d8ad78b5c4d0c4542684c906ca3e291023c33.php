<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>

	<form enctype="multipart/form-data" method="POST" action="addMember">
	<?php echo e(csrf_field()); ?>

	<div>
		<label>Member Name</label><br>
		<textarea name="name"><?php echo e(old('name')); ?></textarea><br>
		<label>Member Designation</label><br>

		<select name="designation" id="">
		<option selected="selected"></option>
		<option value="Professor (Head)">Professor (Head)</option>
		<option value="Professor">Professor</option>
		<option value="Associate Professor">Associate Professor</option>
		<option value="Assistant Professor">Assistant Professor</option>
		<option value="Lecturer">Lecturer</option>
		</select>

		
		<br><br><label>Web Link</label><br>
		<textarea name="link"><?php echo e(old('link')); ?></textarea><br>
		<label>Contact</label><br>
		<textarea name="contact"><?php echo e(old('contact')); ?></textarea><br>
		<label>Pick a Image (least dimension of 300x300)</label><br>
		<input type="file" name="avatar"><br><br>





	</div>

	<div>
		<button type="submit">Add Member</button>
	</div>
	</form>

	<?php if(Session::has('success')): ?>
		<h3 style="color: green"><?php echo e(Session::get('success')); ?></h3>
	<?php endif; ?>

	<?php if(count($errors)): ?>
		<ul>
			<?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
				<li><?php echo e($error); ?></li>
			<?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
		</ul>
	<?php endif; ?>

</body>
</html>