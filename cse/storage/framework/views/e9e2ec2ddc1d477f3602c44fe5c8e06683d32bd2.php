<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>

	<form enctype="multipart/form-data" method="POST" action="addResearch">
	<?php echo e(csrf_field()); ?>

	<div>
		<label>Research Title</label><br>
		<textarea name="title"><?php echo e(old('title')); ?></textarea><br>
		<label>Category</label><br>
		<textarea name="category"><?php echo e(old('category')); ?></textarea><br>
		<label>Web Link (www.example.com)</label><br>
		<textarea name="link"><?php echo e(old('link')); ?></textarea><br>
		<label>Author</label><br>
		<textarea name="author"><?php echo e(old('author')); ?></textarea><br>
	</div>

	<div>
		<button type="submit">Add Research</button>
	</div>
	</form>

	<?php if(count($errors)): ?>
		<ul>
			<?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
				<li><?php echo e($error); ?></li>
			<?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
		</ul>
	<?php endif; ?>

</body>
</html>