<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>CSE Discipline :: Khulna University</title>
<link rel="icon" href="images/title_bar_logo.png">

<link href="/css/stylesheet.css" rel="stylesheet" type="text/css" />
<link href="/css/layerslider.css" rel="stylesheet" type="text/css" />
<link href="/css/prettyPhoto.css" rel="stylesheet" type="text/css" />
<link href="/css/myStylesheet.css" rel="stylesheet" type="text/css" />

<!-- //for bootstrap -->
<!-- Latest compiled and minified CSS -->
<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
		 -->
<!-- Optional theme -->
<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
 -->
<script type='text/javascript' src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script type="text/javascript" src="/js/jquery-easing-1.3.js"></script>
<script type="text/javascript" src="/js/menu.js"></script>
<script type='text/javascript' src="/js/custom.js"></script>
<script type='text/javascript' src="/js/shortcodes.js"></script>
<script type="text/javascript" src="/js/jquery.quicksand.js"></script>
<script src="/js/jquery.prettyPhoto.js" type="text/javascript" charset="utf-8"></script>

<script type="text/javascript" src="/js/my-jquery.js"></script>

</head>
<body>
	<?php echo $__env->yieldContent('header'); ?>

	<?php echo $__env->yieldContent('content'); ?>

	<?php echo $__env->yieldContent('footer'); ?>


	<!-- for bootstrap -->
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> -->
	<!-- Latest compiled and minified JavaScript -->
	<!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
 -->
 	
 </body>
</html>