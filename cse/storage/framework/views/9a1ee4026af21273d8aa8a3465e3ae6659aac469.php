<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>

	<form enctype="multipart/form-data" method="POST" action="updateCricket">
	<?php echo e(method_field('PATCH')); ?>

	<?php echo e(csrf_field()); ?>

	<div>
		<label>Edit the Title</label><br>
		<textarea name="title" style="width: 60%; height: 50%;"><?php echo e($data->title); ?></textarea><br>
		<label>Edit the Description</label><br>
		<textarea name="body" style="width: 60%; height: 50%;"><?php echo e($data->body); ?></textarea><br>
		<label>Update Images</label><br>
		
		<p>Choose images with a least width of 1050px & a least height of 500px</p>
		<label>Choose Picture 1</label><br>
		<input type="file" name="avatar1"><br><br>
		<label>Choose Picture 2</label><br>
		<input type="file" name="avatar2"><br><br>
		<label>Choose Picture 3</label><br>
		<input type="file" name="avatar3"><br><br>
		<label>Choose Picture 4</label><br>
		<input type="file" name="avatar4"><br><br>
		<label>Choose Picture 5</label><br>
		<input type="file" name="avatar5"><br><br>
	</div>

	<div>
		<button type="submit">Save Changes</button>
	</div>
	</form>

	<?php if(count($errors)): ?>
		<ul>
			<?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
				<li><?php echo e($error); ?></li>
			<?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
		</ul>
	<?php endif; ?>

</body>
</html>