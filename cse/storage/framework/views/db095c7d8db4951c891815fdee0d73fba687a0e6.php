<?php $__env->startSection('content'); ?>

<!--Wrapper Start-->
<div id="wrapper">
<div id="page">

<!--Header Start-->
<div id="header">

<div class="logo-part">

<!--Logo Start-->
<div class="logo"><a href="index"><img src="images/logo.png" width="395" height="69" alt="" /></a></div>
<!--Logo End-->

<div class="search-part">

<div class="search">

<!--Search Start-->
<!-- <div class="search-inner">
<input name="textfield" type="text" value="Search Something..." onfocus="if(this.value == 'Search Something...') { this.value = ''; }" onblur="if(this.value == '') { this.value = 'Search Something...'; }" class="search-input-bg" id="textfield"  />
</div> -->
<!--Search End-->

<!--Social Media Start-->
<div class="social-media">
<ul>
<li> <a href="#"><img src="images/facebook.png" width="27" height="28" alt="" /> </a></li>
<li> <a href="#"><img src="images/twitter.png" width="27" height="28" alt="" /> </a></li>
<li> <a href="#"><img src="images/google+.png" width="27" height="28" alt="" /> </a></li>
<li> <a href="#"><img src="images/youtube.png" width="27" height="28" alt="" /> </a></li>
<li> <a href="#"><img src="images/dribbble.png" width="27" height="28" alt="" /> </a></li>
</ul>
</div>
<!--Social Media End-->

</div>

<!--Top Menu Start-->
<div class="top-menu">
<ul>
<li><a href="#">A-Z  Site Index</a></li>
<li><a href="#">Campus Map &amp; Location</a></li>
<li><a href="#">Directory</a></li>
<li><a href="#">Blog</a></li>
<li><a href="#">Help</a></li>
</ul>
</div>
<!--Top Menu End-->

</div>

</div>


<!--Menu Start-->
<div class="menu-part">


<div class="menu">

<div class="menu-inner">
<ul id="main_menu">
	<li><a href="index">Home</a></li>

		<li style="background:none; padding-right:0px;"><a>About Us</a>
			<ul>
				<li><a href="Members">Faculty Members</a></li>
				<li><a href="Staffs">Faculty Staffs</a></li>
				<li><a href="Students">Current Students</a></li>
				<li><a href="Alumni">Alumni</a></li>
			</ul>
		</li>

		<li style="background:none; padding-right:0px;"><a>Academics</a>
			<ul>
				<li><a href="Undergrad">Undergraduate Programs</a></li>
				<li><a href="Grad">Graduate Programs</a></li>
				<li><a href="Research">Research</a></li>
				<!-- <li><a href="Cluster">Cluster</a></li> -->
			</ul>
		</li>

		<li><a href="cluster">Cluster</a></li>
		<li><a href="Sports">Sports</a></li>
		<li><a href="Contacts">Contact Us</a></li>
</ul>
</div>

</div>

<!--Apply Now Part Start-->
<div class="apply"><a href="#"><img src="images/apply-now-buttion.png" width="280" height="60" alt="" /></a></div>
<!--Apply Now Part End-->

</div>
<!--Menu End-->




</div>
<!--Header End-->



<!--Content Start-->
<div id="content">

<div class="inner-part">

<div class="inner-left">

<!-- Staffs List Start -->

<h5>Faculty Stuffs</h5><br>
<div class="testt"></div>
<?php $__currentLoopData = $staffs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $staff): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>

<div class="alllist">
	<img src="/uploads/facultyPeople/staffs/<?php echo e($staff->avatar); ?>" width="150" height="150"/>
	<div class="abc">
	<H3><?php echo e($staff->name); ?></H3>
	<H4><?php echo e($staff->designation); ?></H4>
	<p>Contact: <?php echo e($staff->contact); ?></p><br>
	
	<?php if($staff->link=='#'): ?>
		<a href="" style="font:Normal 12px Arial;color:#716f6f;font-weight:bold;">Know More +</a>
	<?php else: ?>
		<a href="http://<?php echo e($staff->link); ?>" style="font:Normal 12px Arial;color:#716f6f;font-weight:bold;">Know More +</a>
	<?php endif; ?>
	</div>
	
</div>

<?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>

<!-- Staffs List End -->

</div>

<!--Sidebar Part Start-->

<div class="sidebar">

<div class="sidebar-inner">
<div class="sidebar-top"><img src="images/sitebar-top.png" width="269" height="20" alt="" /></div>
<div class="sidebar-center">

<div class="download"><a href="#"><img src="images/download.png" width="202" height="43" alt="" /></a></div>
<div class="event-cal">
<h1>Campus Event Calender<img src="images/rss.png" width="17" height="17" alt="" class="rss" /></h1>

<!-- Calender News Part Start -->

<?php $counter=0; ?>
<?php $__currentLoopData = $events; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index => $event): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>

<?php if($counter===3): ?>
    <?php break; ?>;
<?php endif; ?>
<?php $counter++; ?>  
<a href="Showevents/<?php echo e($event->campusevents_id); ?>">
<div class="event-cal-list" style="background:none;">
<div class="event-cal-image"> <span><?php echo e($months[$index]); ?> <br /><?php echo e($days[$index]); ?></span></div>
<div class="event-cal-text">
<h2><?php echo e($event->title); ?> <span><?php echo e($event->start_time); ?> – <?php echo e($event->end_time); ?></span></h2>
<p><?php echo e($event->heading); ?></p>
</div>
</div>
</a>

<?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>


<!-- Calender News Part End -->




<div class="view-inner"><a href="Campusevents"><img src="images/view.png" width="158" height="37" alt="" /></a></div>
</div>



<!-- Campus News Part Start -->


<div class="cam-news">
<h1>Campus News <img src="images/rss.png" width="17" height="17" alt="" class="rss" /></h1>

<?php $counter=0; ?>
<?php $__currentLoopData = $news; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $paper): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>

<?php if($counter===3): ?>
    <?php break; ?>;
<?php endif; ?>
<?php $counter++; ?>  

<a href="Shownews/<?php echo e($paper->campusnews_id); ?>">
<h2><img src="uploads/avatars/<?php echo e($paper->avatar); ?>" width="209" height="105" alt="" /></h2>
<h3><?php echo e($paper->title); ?></h3>
<p><?php echo e($paper->heading); ?></p>
</a>

<?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>

<p><a href="Campusnews" class="view_full_list">View Full List</a></p>

</div>

<!-- Campus News Part End -->

</div>
<div class="sidebar-center"><img src="images/sitebar-bottom.png" width="269" height="20" alt="" /></div>

</div>

</div>

<!--Sidebar Part End-->


</div>






</div>
<!--Content End-->



</div>

<!--Footer Start-->
<div id="footer">

<div class="footer-top">
<div class="footer-top-inner">

<div class="footer-menu">

<!--Footer Menu1 Start-->

<div class="general">
<h1>General info</h1>
<ul>
<li><a href="#">Visit</a></li>
<li><a href="#">Maps</a></li>
<li><a href="#">History </a></li>
<li><a href="#">Diversity</a></li>
<li><a href="#">Administration</a></li>
<li><a href="#">Accreditation Status</a></li>
</ul>
</div>

<!--Footer Menu1 End-->

<!--Footer Menu2 Start-->
<div class="university">
<h1>Wieldstrem University</h1>
<ul>
<li><a href="#">Blackboard</a></li>
<li><a href="#">Calendars</a></li>
<li><a href="#">Class Schedule </a></li>
<li><a href="#">Colonel's Compass</a></li>
<li><a href="#">Green Initiatives</a></li>
<li><a href="#">Faculty/Staff Email</a></li>
</ul>
</div>
<!--Footer Menu2 End-->

<!--Footer Menu3 Start-->
<div class="quick">
<h1>Quick Link</h1>
<ul>
<li><a href="#">Colleges &amp; Departments</a></li>
<li><a href="#">Community Education</a></li>
<li><a href="#">Conferencing &amp; Events </a></li>
<li><a href="#">Employment &amp; Benefits</a></li>
<li><a href="#">Financial Aid</a></li>
<li><a href="#">Information Technology</a></li>
</ul>
</div>
<!--Footer Menu3 End-->

<!--Footer About Part Start-->
<div class="about">
<h1>About Wieldstrem University</h1>
<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.</p>
</div>
<!--Footer About Part End-->

</div>

<div class="footer-bottom">

<!--Copyright Part Start-->
<div class="footer-copy">Copyright &copy; 2012 Wieldstrem University. All Rights Reserved.<br />Wieldstrem University, Strem Valley , Stoviana 258963.<br />
<b>Call :  1800-895-985 <a href="#">|</a> Fax  :  859 582 6975 <a href="#">|</a> Email  :  info@wieldstremuniversity.com</b>
</div>
<!--Copyright Part End-->

<!--Footer logo Part Start-->
<div class="footer-logo"><img src="images/footer-logo.png" width="262" height="57" alt="" /></div>
<!--Footer logo Part End-->

</div>

</div>
</div>


</div>
<!--Footer End-->

</div>
<div id="backtotop"><a href="#"></a></div>
<!--Wrapper End-->

<script type="text/javascript">
var main_menu=new main_menu.dd("main_menu");
main_menu.init("main_menu","menuhover");
</script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>