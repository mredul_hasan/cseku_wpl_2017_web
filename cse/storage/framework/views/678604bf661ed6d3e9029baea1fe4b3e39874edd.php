<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>

	<form enctype="multipart/form-data" method="POST" action="uploadNews">
	<?php echo e(csrf_field()); ?>

	<div>
		<label>Give a Title</label><br>
		<textarea name="title"><?php echo e(old('title')); ?></textarea><br>
		<label>Put a Heading</label><br>
		<textarea name="heading"><?php echo e(old('heading')); ?></textarea><br>
		<label>Fill out the Body</label><br>
		<textarea name="body"><?php echo e(old('body')); ?></textarea><br>
		<label>Pick a Image</label><br>
		<input type="file" name="avatar"><br><br>
	</div>

	<div>
		<button type="submit">Add News</button>
	</div>
	</form>

	<?php if(Session::has('success')): ?>
		<h3 style="color: green"><?php echo e(Session::get('success')); ?></h3>
	<?php endif; ?>

	<?php if(count($errors)): ?>
		<ul>
			<?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
				<li><?php echo e($error); ?></li>
			<?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
		</ul>
	<?php endif; ?>

</body>
</html>