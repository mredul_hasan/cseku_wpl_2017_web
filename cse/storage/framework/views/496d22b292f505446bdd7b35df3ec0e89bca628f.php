<?php $__env->startSection('content'); ?>
<h1>Campus News</h1>
<h2><?php echo e($news->title); ?></h2>
<h4>Posted at . <?php echo e($news->created_at); ?></h4><h4>Last Updated at . <?php echo e($news->updated_at); ?></h4><br>
<div><img src="/uploads/avatars/<?php echo e($news->avatar); ?>" width="300" height="300" alt="" /></div>
<p><?php echo e($news->body); ?></p>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>