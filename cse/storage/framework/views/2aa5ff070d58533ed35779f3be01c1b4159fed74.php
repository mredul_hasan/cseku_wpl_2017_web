<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>

	<form enctype="multipart/form-data" method="POST" action="updateUndergrad">
	<?php echo e(method_field('PATCH')); ?>

	<?php echo e(csrf_field()); ?>

	<div>
		<label>Edit the Title</label><br>
		<textarea name="title" style="width: 60%; height: 50%;"><?php echo e($data->title); ?></textarea><br>
		<label>Edit the Description</label><br>
		<textarea name="body" style="width: 60%; height: 50%;"><?php echo e($data->body); ?></textarea><br>
		<label>Update Image</label><br>
		
		<p>Choose an image with a least width of 1050px & a least height of 500px</p>
		<label>Choose Picture</label><br>
		<input type="file" name="avatar"><br><br>

		<label>Update Syllabus</label><br>
		
		<p>Choose a PDF File</p>
		<label>Choose File</label><br>
		<input type="file" name="syllabus"><br><br>
	</div>

	<div>
		<button type="submit">Save Changes</button>
	</div>
	</form>

	<?php if(Session::has('success')): ?>
		<h3 style="color: green"><?php echo e(Session::get('success')); ?></h3>
	<?php endif; ?>

	<?php if(count($errors)): ?>
		<ul>
			<?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
				<li><?php echo e($error); ?></li>
			<?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
		</ul>
	<?php endif; ?>

</body>
</html>