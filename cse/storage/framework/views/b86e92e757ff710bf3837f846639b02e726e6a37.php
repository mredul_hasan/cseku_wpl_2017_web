<?php $__env->startSection('content'); ?>

<!--Wrapper Start-->
<div id="wrapper">
<div id="page">

<!--Header Start-->
<div id="header">

<div class="logo-part">

<!--Logo Start-->
<div class="logo"><a href="index"><img src="images/logo.png" width="395" height="69" alt="" /></a></div>
<!--Logo End-->

<div class="search-part">

<div class="search">

<!--Search Start-->
<!-- <div class="search-inner">
<input name="textfield" type="text" value="Search Something..." onfocus="if(this.value == 'Search Something...') { this.value = ''; }" onblur="if(this.value == '') { this.value = 'Search Something...'; }" class="search-input-bg" id="textfield"  />
</div> -->
<!--Search End-->

<!--Social Media Start-->
<div class="social-media">
<ul>
<li> <a href="#"><img src="images/facebook.png" width="27" height="28" alt="" /> </a></li>
<li> <a href="#"><img src="images/twitter.png" width="27" height="28" alt="" /> </a></li>
<li> <a href="#"><img src="images/google.png" width="27" height="28" alt="" /> </a></li>
<li> <a href="#"><img src="images/youtube.png" width="27" height="28" alt="" /> </a></li>
<li> <a href="#"><img src="images/dribbble.png" width="27" height="28" alt="" /> </a></li>
</ul>
</div>
<!--Social Media End-->

</div>

<!--Top Menu Start-->
<div class="top-menu">
<ul>
<li><a href="#">A-Z  Site Index</a></li>
<li><a href="#">Campus Map &amp; Location</a></li>
<li><a href="#">Directory</a></li>
<li><a href="#">Blog</a></li>
<li><a href="#">Help</a></li>
</ul>
</div>
<!--Top Menu End-->

</div>

</div>


<!--Menu Start-->
<div class="menu-part">


<div class="menu">

<div class="menu-inner">
<ul id="main_menu">
	   <li><a href="index">Home</a></li>

		<li style="background:none; padding-right:0px;"><a>About Us</a>
			<ul>
				<li><a href="Members">Faculty Members</a></li>
				<li><a href="Staffs">Faculty Staffs</a></li>
				<li><a href="Students">Current Students</a></li>
				<li><a href="Alumni">Alumni</a></li>
			</ul>
		</li>

		<li style="background:none; padding-right:0px;"><a>Academics</a>
			<ul>
				<li><a href="Undergrad">Undergraduate Programs</a></li>
				<li><a href="Grad">Graduate Programs</a></li>
				<li><a href="Research">Research</a></li>
				<!-- <li><a href="Cluster">Cluster</a></li> -->
			</ul>
		</li>

		<li><a href="cluster">Cluster</a></li>
		<li><a href="Sports">Sports</a></li>
		<li><a href="Contacts">Contact Us</a></li>
</ul>
</div>

</div>

<!--Apply Now Part Start-->
<div class="apply"><a href="index"><img src="images/apply-now-buttion.png" width="280" height="60" alt="" /></a></div>
<!--Apply Now Part End-->

</div>
<!--Menu End-->




</div>
<!--Header End-->



<!--Content Start-->
<div id="content">

<div class="inner-part">

<!--Slider Part Start-->
<div class="banner-part">

<div id="outerslider">
        	<div class="container">
        	<div id="slidercontainer" class="twelve columns">
            
            	<section id="slider">
                	<div class="line-op"></div>
                    <div id="layerslider" class="slideritems">
                    
                    	<div class="ls-layer" id="ls-layer-1" data-rel="slidedelay: 3000;">
                            <img class="ls-bg" src="/uploads/StdAlm/students/<?php echo e($data->avatar1); ?>" alt="layer" />
                        </div>
                        
                        <div class="ls-layer" id="ls-layer-2" data-rel="slidedelay: 3000;">
                          
                            <img class="ls-bg" src="/uploads/StdAlm/students/<?php echo e($data->avatar2); ?>" alt="layer" />
                            
                        </div>
                        
                        <div class="ls-layer" id="ls-layer-3" data-rel="slidedelay: 3000;">
                            
                            <img class="ls-bg" src="/uploads/StdAlm/students/<?php echo e($data->avatar3); ?>" alt="layer" />
                           

                        </div>
                        
                        <div class="ls-layer" id="ls-layer-4" data-rel="slidedelay: 3000;">
                            
                            <img class="ls-bg" src="/uploads/StdAlm/students/<?php echo e($data->avatar4); ?>" alt="layer" />
                         

                        </div>

                        <div class="ls-layer" id="ls-layer-4" data-rel="slidedelay: 3000;">
                            
                            <img class="ls-bg" src="/uploads/StdAlm/students/<?php echo e($data->avatar5); ?>" alt="layer" />
                         

                        </div>
                        
                    </div>
                    
                </section>
                
            </div>
            </div>
        </div>

</div>
<!--Slider Part End-->

<div class="inner-left">

<h1><?php echo e($data->title); ?></h1>

<h4><?php echo e($data->body); ?></h4>



</div>


</div>


</div>
<!--Content End-->



</div>

<!--Footer Start-->
<div id="footer">

<div class="footer-top">
<div class="footer-top-inner">

<div class="footer-menu">

<!--Footer Menu1 Start-->

<div class="general">
<h1>General info</h1>
<ul>
<li><a href="#">Visit</a></li>
<li><a href="#">Maps</a></li>
<li><a href="#">History </a></li>
<li><a href="#">Diversity</a></li>
<li><a href="#">Administration</a></li>
<li><a href="#">Accreditation Status</a></li>
</ul>
</div>

<!--Footer Menu1 End-->

<!--Footer Menu2 Start-->
<div class="university">
<h1>Wieldstrem University</h1>
<ul>
<li><a href="#">Blackboard</a></li>
<li><a href="#">Calendars</a></li>
<li><a href="#">Class Schedule </a></li>
<li><a href="#">Colonel's Compass</a></li>
<li><a href="#">Green Initiatives</a></li>
<li><a href="#">Faculty/Staff Email</a></li>
</ul>
</div>
<!--Footer Menu2 End-->

<!--Footer Menu3 Start-->
<div class="quick">
<h1>Quick Link</h1>
<ul>
<li><a href="#">Colleges &amp; Departments</a></li>
<li><a href="#">Community Education</a></li>
<li><a href="#">Conferencing &amp; Events </a></li>
<li><a href="#">Employment &amp; Benefits</a></li>
<li><a href="#">Financial Aid</a></li>
<li><a href="#">Information Technology</a></li>
</ul>
</div>
<!--Footer Menu3 End-->

<!--Footer About Part Start-->
<div class="about">
<h1>About Wieldstrem University</h1>
<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.</p>
</div>
<!--Footer About Part End-->

</div>

<div class="footer-bottom">

<!--Copyright Part Start-->
<div class="footer-copy">Copyright &copy; 2016 CSE Discipline, Khulna University.<br />All Rights Reserved.<br />
<b>Khulna University, Khulna - 9208.</b>
</div>
<!--Copyright Part End-->

<!--Footer logo Part Start-->
<div class="footer-logo"><img src="images/footer-logo.png" width="262" height="57" alt="" /></div>
<!--Footer logo Part End-->

</div>

</div>
</div>


</div>
<!--Footer End-->

</div>
<div id="backtotop"><a href="#"></a></div>
<!--Wrapper End-->

<!-- jQuery Layerslider -->
<script type="text/javascript" src="js/layerslider.kreaturamedia.jquery-min.js"></script>
<script type="text/javascript">
jQuery(window).load(function() {
    jQuery('#layerslider.slideritems').layerSlider({
		skinsPath : 'images/layerslider-skins/',
		skin : 'smartgroup',
		autoStart : true
	});
});
</script>

<script type="text/javascript">
var main_menu=new main_menu.dd("main_menu");
main_menu.init("main_menu","menuhover");
</script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>