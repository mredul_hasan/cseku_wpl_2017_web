<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>

	<form enctype="multipart/form-data" method="POST" action="">
	<?php echo e(method_field('PATCH')); ?>

	<?php echo e(csrf_field()); ?>

	<div>
		<label>Edit the Title</label><br>
		<textarea name="title" style="width: 60%; height: 50%;"><?php echo e($contact->title); ?></textarea><br>
		<label>Edit the Heading</label><br>
		<textarea name="heading" style="width: 60%; height: 50%;"><?php echo e($contact->heading); ?></textarea><br>
		<label>Update Contacts</label><br>
		<textarea name="call" style="width: 60%; height: 50%;"><?php echo e($contact->call); ?></textarea><br>
		<label>Update Fax Number</label><br>
		<textarea name="fax" style="width: 60%; height: 50%;"><?php echo e($contact->fax); ?></textarea><br>
		<label>Update Email</label><br>
		<textarea name="email" style="width: 60%; height: 50%;"><?php echo e($contact->email); ?></textarea><br>
	</div>

	<div>
		<button type="submit">Save Changes</button>
	</div>
	</form>

	<?php if(Session::has('success')): ?>
		<h3 style="color: green"><?php echo e(Session::get('success')); ?></h3>
	<?php endif; ?>

	<?php if(count($errors)): ?>
		<ul>
			<?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
				<li><?php echo e($error); ?></li>
			<?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
		</ul>
	<?php endif; ?>

</body>
</html>