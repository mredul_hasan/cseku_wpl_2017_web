<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>

	<form enctype="multipart/form-data" method="POST" action="uploadEvents">
	<?php echo e(csrf_field()); ?>

	<div>
		<label>Give a Title</label><br>
		<textarea name="title"><?php echo e(old('title')); ?></textarea><br>
		<label>Put a Heading</label><br>
		<textarea name="heading"><?php echo e(old('heading')); ?></textarea><br>
		<label>Fill out the Body</label><br>
		<textarea name="body"><?php echo e(old('body')); ?></textarea><br>
		<label>Set a Starting Date & Time</label><br><br>
		<input type="Date" name="starting_date"><input type="time" name="starting_time"><br><br>
		<label>Set a Ending Date & Time</label><br><br>
		<input type="Date" name="ending_date"><input type="time" name="ending_time"><br><br>
		<label>Pick a Image</label><br>
		<input type="file" name="avatar"><br><br>
	</div>

	<div>
		<button type="submit">Add Event</button>
	</div>
	</form>

	<?php if(Session::has('success')): ?>
		<h3 style="color: green"><?php echo e(Session::get('success')); ?></h3>
	<?php endif; ?>

	<?php if(count($errors)): ?>
		<ul>
			<?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
				<li><?php echo e($error); ?></li>
			<?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
		</ul>
	<?php endif; ?>

</body>
</html>