<?php $__env->startSection('content'); ?>

<!--Wrapper Start-->
<div id="wrapper">
<div id="page">

<!--Header Start-->
<div id="header">

<div class="logo-part">

<!--Logo Start-->
<div class="logo"><a href="index"><img src=<?php echo e(url("images/logo.png")); ?> width="450" height="90" alt="" /></a></div>
<!--Logo End-->

<div class="search-part">

<div class="search">

<!--Search Start-->
<!-- <div class="search-inner">
<input name="textfield" type="text" value="Search Something..." onfocus="if(this.value == 'Search Something...') { this.value = ''; }" onblur="if(this.value == '') { this.value = 'Search Something...'; }" class="search-input-bg" id="textfield"  />
</div> -->
<!--Search End-->

<!--Social Media Start-->
<div class="social-media">
<ul>
<li> <a href="#"><img src="images/facebook.png" width="27" height="28" alt="" /> </a></li>
<li> <a href="#"><img src="images/twitter.png" width="27" height="28" alt="" /> </a></li>
<li> <a href="#"><img src="images/google.png" width="27" height="28" alt="" /> </a></li>
<li> <a href="#"><img src="images/youtube.png" width="27" height="28" alt="" /> </a></li>
<li> <a href="#"><img src="images/dribbble.png" width="27" height="28" alt="" /> </a></li>
</ul>
</div>
<!--Social Media End-->

</div>

<!--Top Menu Start-->
<div class="top-menu">
<ul>
<li><a href="#">A-Z  Site Index</a></li>
<li><a href="#">Campus Map &amp; Location</a></li>
<li><a href="#">Directory</a></li>
<li><a href="#">Blog</a></li>
<li><a href="#">Help</a></li>
</ul>
</div>
<!--Top Menu End-->

</div>

</div>


<!--Menu Start-->
<div class="menu-part">


<div class="menu">

<div class="menu-inner">
<ul id="main_menu">
    <li><a href="index">Home</a></li>

        <li style="background:none; padding-right:0px;"><a>About Us</a>
            <ul>
                <li><a href=<?php echo e(url("Members")); ?>>Faculty Members</a></li>
                <li><a href="Staffs">Faculty Staffs</a></li>
                <li><a href="Students">Current Students</a></li>
                <li><a href="Alumni">Alumni</a></li>
            </ul>
        </li>

        <li style="background:none; padding-right:0px;"><a>Academics</a>
            <ul>
                <li><a href="Undergrad">Undergraduate Programs</a></li>
                <li><a href="Grad">Graduate Programs</a></li>
                <li><a href="Research">Research</a></li>
                <!-- <li><a href="Cluster">Cluster</a></li> -->
            </ul>
        </li>

        <li><a href="cluster">Cluster</a></li>
        <li><a href="Sports">Sports</a></li>
        <li><a href="Contacts">Contact Us</a></li>
</ul>
</div>

</div>

<!--Apply Now Part Start-->
<div class="apply"><a href="index"><img src=<?php echo e(url('/images/apply-now-buttion.png')); ?> width="280" height="60" alt="" /></a></div>
<!--Apply Now Part End-->

</div>
<!--Menu End-->

<!--Banner Part Start-->
<div class="banner-part">

<div id="outerslider">
        	<div class="container">
        	<div id="slidercontainer" class="twelve columns">
            
            	<section id="slider">
                	<div class="line-op"></div>
                    <div id="layerslider" class="slideritems">
                    
                    	<div class="ls-layer" id="ls-layer-1" data-rel="slidedelay: 3000;">
                            <img class="ls-bg" src="images/banner1.png" alt="layer" />
                            <h1 class="ls-s3" id="slide1text1">Change your Education</h1>
                            <p class="slidetext ls-s4" id="slide1p1" >Lorem Ipsum is simply dummy text of the printing and typesetting industry.  </p>
                        </div>
                        
                        <div class="ls-layer" id="ls-layer-2" data-rel="slidedelay: 3000;">
                          
                            <img class="ls-bg" src="images/banner2.png" alt="layer" />
                            <h1 class="ls-s3" id="slide2text1">New Way Yours</h1>
                            <p class="slidetext ls-s4" id="slide2p1">Lorem Ipsum is simply dummy text of the printing and typesetting industry.  </p>
                        </div>
                        
                        <div class="ls-layer" id="ls-layer-3" data-rel="slidedelay: 3000;">
                            
                            <img class="ls-bg" src="images/banner3.png" alt="layer" />
                            <h1 class="ls-s3" id="slide3text1" >University experience</h1>
                            <p class="slidetext ls-s4" id="slide3p1">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>

                        </div>
                        
                        <div class="ls-layer" id="ls-layer-4" data-rel="slidedelay: 3000;">
                            
                            <img class="ls-bg" src="images/banner4.png" alt="layer" />
                            <h1 class="ls-s3" id="slide3text1" >We Are Offer Course</h1>
                            <p class="slidetext ls-s4" id="slide3p1" >Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>

                        </div>
                        
                    </div>
                    
                </section>
                
            </div>
            </div>
        </div>

</div>
<!--Banner Part End-->

</div>
<!--Header End-->



<!--Content Start-->
<div id="content">

<!--Welcome Part Start-->
<div class="welcome-part">

<!--Welcome Start-->
<div class="welcome">
<h1>Welcome to Khulna University</h1>
<p><img src="images/college.png" width="241" height="119" alt="" class="univercity-image" /> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make galley  industry's standard of type a type specimen book.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since. <br /><br />

There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised 

<span class="more_text" style="display: none;">
    Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make galley  industry's standard of type a type specimen book.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since.<br>
    <a href="#" class="view_less">View Less</a>
</span>

  <a href="#" class="view_more">View More</a></p>
</div>
<!--Welcome End-->



</div>
<!--Welcome Part End-->


<!--Event Part Start-->
<div class="event-part">


<!--Campus News Part Start-->
<div class="news">
<h1>Campus News<img src="images/rss.png" width="17" height="17" alt="" class="rss" /></h1>



<!-- show the first 3 news start -->
<?php $counter=0; ?>
<?php $__currentLoopData = $news; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $paper): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>

<?php if($counter===3): ?>
    <?php break; ?>;
<?php endif; ?>
<?php $counter++; ?>  

<div class="news-list">
<a href="Shownews/<?php echo e($paper->campusnews_id); ?>">
<div class="news-image"><img src="/uploads/avatars/<?php echo e($paper->avatar); ?>" width="75" height="61" alt="" /></div>
<div class="news-text">
<h2><?php echo e($paper->title); ?><span><?php echo e($paper->created_at); ?></span></h2>
<p><?php echo e($paper->heading); ?></p>
</div>
</a>
</div>
<?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>

<!-- show the first 3 news end -->



<a href="Campusnews" class="view_full_list">View Full List</a>

</div>
<!--Campus News Part End-->

<!--Campus Event Calender Part Start-->

<div class="calender">
<h1>Campus Event Calender<img src="images/rss.png" width="17" height="17" alt="" class="rss" /></h1>


<!--show the first 3 events Start-->

<?php $counter=0; ?>
<?php $__currentLoopData = $events; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $index => $event): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>

<?php if($counter===3): ?>
    <?php break; ?>;
<?php endif; ?>
<?php $counter++; ?>  

<div class="calender-list">
<a href="Showevents/<?php echo e($event->campusevents_id); ?>">
    <div class="calender-image"> <span><?php echo e($months[$index]); ?> <br /><?php echo e($days[$index]); ?></span></div>
<div class="calender-text">
<h2><?php echo e($event->title); ?> <span><?php echo e($event->start_time); ?> – <?php echo e($event->end_time); ?></span></h2>
<p><a href="Showevents/<?php echo e($event->campusevents_id); ?>"><?php echo e($event->heading); ?></a></p>
</div>
</a>
</div>

<?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>

<!--show the first 3 events End-->


<div class="view"><a href="Campusevents"><img src="images/view.png" width="158" height="37" alt="" /></a></div>

</div>
<!--Campus Event Calender Part End-->


<!--Spotlight Start-->
<div class="spotlight">
<h1>Campus Spotlight <img src="images/rss.png" width="17" height="17" alt="" class="rss" /></h1>
<a href="spotlight">
    <h2><img src="uploads/spotlight/<?php echo e($spotlight->avatarCover); ?>" width="305" height="200" alt="" /></h2>
</a>
<p><?php echo e($spotlight->heading); ?><a href="spotlight">Read More +</a></p>
</div>
<!--Spotlight End-->


</div>
<!--Event Part End-->

<!--Moments Part Start-->
<!-- <div class="moments-part">
<h1>Campus  Beautiful  Moments <img src="images/rss.png" width="17" height="17" alt="" class="rss" /></h1>

<ul>


<li><div class="gallery_item"><a href="images/mo-image-big-2.jpg" rel="prettyPhoto[gallery1]" ><img src="images/mo-image2.png" width="284" height="127" alt="" /></a><span><a href="images/mo-image-big-2.jpg" rel="prettyPhoto[gallery]" ><img src="images/photo.png" alt="" title="" /></a></span></div></li>

<li><div class="gallery_item"><a href="images/mo-image-big-3.jpg" rel="prettyPhoto[gallery1]" ><img src="images/mo-image3.png" width="284" height="127" alt="" /></a><span><a href="images/mo-image-big-3.jpg" rel="prettyPhoto[gallery]" ><img src="images/photo.png" alt="" title="" /></a></span></div></li>

<li><div class="gallery_item"><a href="http://vimeo.com/173714" rel="prettyPhoto[gallery1]" ><img src="images/mo-image1.png" width="284" height="127" alt="" /></a><span><a href="http://vimeo.com/173714" rel="prettyPhoto[gallery]" ><img src="images/photo.png" alt="" title="" /></a></span></div></li>

</ul>

</div> -->
<!--Moments Part End-->

</div>
<!--Content End-->



</div>

<!--Footer Start-->
<div id="footer">

<div class="footer-top">
<div class="footer-top-inner">

<div class="footer-menu">

<!--Footer Menu1 Start-->

<div class="general">
<h1>General info</h1>
<ul>
<li><a href="#">Visit</a></li>
<li><a href="#">Maps</a></li>
<li><a href="#">History </a></li>
<li><a href="#">Diversity</a></li>
<li><a href="#">Administration</a></li>
<li><a href="#">Accreditation Status</a></li>
</ul>
</div>

<!--Footer Menu1 End-->

<!--Footer Menu2 Start-->
<div class="university">
<h1>Wieldstrem University</h1>
<ul>
<li><a href="#">Blackboard</a></li>
<li><a href="#">Calendars</a></li>
<li><a href="#">Class Schedule </a></li>
<li><a href="#">Colonel's Compass</a></li>
<li><a href="#">Green Initiatives</a></li>
<li><a href="#">Faculty/Staff Email</a></li>
</ul>
</div>
<!--Footer Menu2 End-->

<!--Footer Menu3 Start-->
<div class="quick">
<h1>Quick Link</h1>
<ul>
<li><a href="#">Colleges &amp; Departments</a></li>
<li><a href="#">Community Education</a></li>
<li><a href="#">Conferencing &amp; Events </a></li>
<li><a href="#">Employment &amp; Benefits</a></li>
<li><a href="#">Financial Aid</a></li>
<li><a href="#">Information Technology</a></li>
</ul>
</div>
<!--Footer Menu3 End-->

<!--Footer About Part Start-->
<div class="about">
<h1>About Wieldstrem University</h1>
<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.</p>
</div>
<!--Footer About Part End-->

</div>

<div class="footer-bottom">

<!--Copyright Part Start-->
<div class="footer-copy">Copyright &copy; 2016 CSE Discipline, Khulna University.<br />All Rights Reserved.<br />
<b>Khulna University, Khulna - 9208.</b>
</div>
<!--Copyright Part End-->

<!--Footer logo Part Start-->
<div class="footer-logo"><img src="images/footer-logo.png" width="262" height="57" alt="" /></div>
<!--Footer logo Part End-->

</div>

</div>
</div>


</div>
<!--Footer End-->

</div>

<div id="backtotop"><a href="#"></a></div>
<!--Wrapper End-->

<!-- jQuery Layerslider -->
<script type="text/javascript" src="js/layerslider.kreaturamedia.jquery-min.js"></script>
<script type="text/javascript">
jQuery(window).load(function() {
    jQuery('#layerslider.slideritems').layerSlider({
		skinsPath : 'images/layerslider-skins/',
		skin : 'smartgroup',
		autoStart : true
	});
});
</script>

<script type="text/javascript">
var main_menu=new main_menu.dd("main_menu");
main_menu.init("main_menu","menuhover");
</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>