<?php $__env->startSection('content'); ?>
<h1>Campus Events</h1>
<h2><?php echo e($events->title); ?></h2>
<h4>Posted at . <?php echo e($events->created_at); ?></h4><h4>Last Updated at . <?php echo e($events->updated_at); ?></h4><br>
<div><img src="/uploads/avatars/<?php echo e($events->avatar); ?>" width="300" height="300" alt="" /></div>
<p><?php echo e($events->body); ?></p>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layout', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>