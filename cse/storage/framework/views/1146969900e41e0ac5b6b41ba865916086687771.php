<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>

	<form enctype="multipart/form-data" method="POST" action="addStaff">
	<?php echo e(csrf_field()); ?>

	<div>
		<label>Staff Name</label><br>
		<textarea name="name"><?php echo e(old('name')); ?></textarea><br>
		<label>Staff Designation</label><br>
		<textarea name="designation"><?php echo e(old('designation')); ?></textarea><br>
		<label>Web Link (www.example.com)</label><br>
		<textarea name="link"><?php echo e(old('link')); ?></textarea><br>
		<label>Contact</label><br>
		<textarea name="contact"><?php echo e(old('contact')); ?></textarea><br>
		<label>Pick a Image (least dimension of 300x300)</label><br>
		<input type="file" name="avatar"><br><br>
	</div>

	<div>
		<button type="submit">Add Staff</button>
	</div>
	</form>

	<?php if(count($errors)): ?>
		<ul>
			<?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getFirstLoop(); ?>
				<li><?php echo e($error); ?></li>
			<?php endforeach; $__env->popLoop(); $loop = $__env->getFirstLoop(); ?>
		</ul>
	<?php endif; ?>

</body>
</html>